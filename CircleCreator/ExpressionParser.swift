//
//  ExpressionParser.swift
//  CircleCreator
//
//  Created by r00t on 29.06.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class ExpressionParser<T> {
    
    typealias Arguments = [String: T]
    
    private var formula: String!
    private var arguments: Arguments
    
    init(formula: String, arguments: Arguments) {
        self.formula = formula
        self.arguments = arguments
    }
    
    func parse() -> T {
        guard let value = formula.expression.expressionValue(with: arguments, context: nil) as? T else {
            fatalError()
        }
        return value
    }
    
    

}

fileprivate extension String {
     var expression: NSExpression {
        return NSExpression(format: self)
    }
}
