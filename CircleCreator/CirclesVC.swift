//
//  ViewController.swift
//  CircleCreator
//
//  Created by r00t on 29.06.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class CirclesVC: UIViewController {
    
    typealias JSON = [String: Any]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getJson { self.parse($0) }
    }
    
    /// Getting Json from local file
    ///
    /// - Parameter answer: async return json object
    func getJson(answer: (JSON) -> ()) {
        Bundle.main.path(forResource: "Circle", ofType: "json")
        .flatMap { NSData(contentsOfFile: $0) }.flatMap(Data.init)
            .map {
                do {
                   let json = try JSONSerialization.jsonObject(with: $0, options: .mutableContainers) as? JSON ?? [:]
                    answer(json)
                } catch {
                    print(error)
                }
                }
        
    }
    
    /// Parsing Json object
    ///
    /// - Parameter json: [String: Any] type
    func parse(_ json: JSON) {
        var argumentsDictionary = [String: Double]()
        
        json["outputValues"].flatMap { $0 as? [[String: Any]] }?
            .map { ($0["param"] as? String ?? "", $0["value"] as? Double ?? 0) }
            .forEach { param, value in
                argumentsDictionary[param] = value
        }
        
        let formula = json["formula"] as? String ?? ""
        
        let circlesCount = json["circlesCount"] as? Int ?? 0
        
        let firstValue = argumentsDictionary["X1"] ?? 0
        
        self.calculateCircleExpression(formula: formula, circle: circlesCount, arg: argumentsDictionary, firstValue: firstValue)
        
    }
    
    
    /// Calulate expression method
    ///
    /// - Parameters:
    ///   - formula: Expression formula to calculate
    ///   - Number: number of cirlcles to calculate radiuses and draw
    ///   - arg: arguments to formula
    ///   - firstValue: x1
    func calculateCircleExpression(formula: String, circle Number: Int, arg: [String: Double], firstValue: Double) {
        var radiuses = [CGFloat(firstValue)]
        
        for number in 0..<Number {
            let newFormula = formula.replacingOccurrences(of: "X(n-1)", with: String(describing: radiuses[number]))
            let expressionParser = ExpressionParser(formula: newFormula, arguments: arg).parse()
            
            radiuses.append(CGFloat(expressionParser))
        }

        
        self.drawCircles(with: radiuses)
    }
    
    /// Method that performs drawing on view
    ///
    /// - Parameter Radiuses: array of circles radiuses
    func drawCircles(with Radiuses: [CGFloat]) {
        print(Radiuses)
        let circleView = CirclesView(frame: self.view.bounds, circlesRadiuses: Radiuses, drawPosition: .center)
        self.view.addSubview(circleView)
    }
    
    
    


}

