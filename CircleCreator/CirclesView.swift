//
//  CirclesView.swift
//  CircleCreator
//
//  Created by r00t on 29.06.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class CirclesView: UIView {
    
    // CIRCLES POSITION ON VIEW
    enum DrawPosition {
        case center
        
        func value(from View: UIView) -> CGPoint {
            switch self {
            case .center: return View.center
            }
        }
    }
    
    // PROPS
    private var drawPosition: DrawPosition = .center
    private var circlesRadiuses: [CGFloat]! {
        didSet {
            self.drawCircles(with: self.circlesRadiuses)
        }
    }
    
    
    // INIT'S
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, circlesRadiuses: [CGFloat], drawPosition: DrawPosition) {
        self.init(frame: frame)
        self.circlesRadiuses = circlesRadiuses
        self.drawPosition = drawPosition
        self.backgroundColor = UIColor.white
        
        self.drawCircles(with: self.circlesRadiuses)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    /// Method which is drawing circles on the view.
    ///
    /// - Parameter Radiuses: Array of circles radiuses
    
    func drawCircles(with Radiuses: [CGFloat]) {
        self.cleanShapes()
        print(self.drawPosition.value(from: self))
        Radiuses.forEach { radius in
            let circle = Circle(radius: radius, center: self.drawPosition.value(from: self))
            self.layer.addSublayer(circle)
        }
    }
    
    
    
    /// ShapeLayer cleaner, to avoid duplicate shapes on screen, and perform multiple drawings
    private func cleanShapes() {
        self.layer.sublayers?.filter { $0 is CAShapeLayer }
                                .forEach { $0.removeFromSuperlayer() }
    }

}
