//
//  Circle.swift
//  CircleCreator
//
//  Created by r00t on 29.06.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class Circle: CAShapeLayer {
    
    private var radius: CGFloat!
    private var center: CGPoint!
    
    
    init(radius: CGFloat, center: CGPoint) {
        super.init()
        self.radius = radius
        self.center = center
        self.setupCircle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCircle() {
        let drawPath = UIBezierPath(arcCenter: center,
                                    radius: radius,
                                    startAngle: 0,
                                    endAngle: CGFloat(Float.pi * 2),
                                    clockwise: true)
        
        self.path = drawPath.cgPath
        
        self.fillColor = UIColor.clear.cgColor
        
        self.strokeColor = UIColor.randomColor.cgColor
        
        self.lineWidth = 2
    }

}

// MARK: - Generate random color drand48 - value from 0-1
fileprivate extension UIColor {
    static var randomColor: UIColor {
        var randomNumber: CGFloat {
            return CGFloat(drand48())
        }
        return UIColor(red: randomNumber, green: randomNumber, blue: randomNumber, alpha: 1.0)
    }
}
